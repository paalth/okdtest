* Deploy OKD 3.11
  * Use Ansible playbooks for install
  * Use httpd auth
  * Use cloud-provider integration
* Use official CentOS 7 image
* DNS - use cloud provider auto-issued names or nip.io
* 1 bastion node
* 1 all-in-one OKD node
* Execute same process on all providers
* Use cloud provider native automation
  * AWS - [Cloud Formation](https://docs.aws.amazon.com/cloudformation/index.html)
  * Azure - [Resource Manager](https://docs.microsoft.com/en-us/azure/templates/)
  * GCP - [Deployment Manager](https://cloud.google.com/deployment-manager/docs/)
* Process
  * Create infrastructure
    * Run automation to create required resources
    * Create bastion node
    * Create master/all-in-one node
    * Create security group(s)
  * Config infrastructure
    * Execute bastion config script
    * Execute master config playbook
  * Install OKD
    * Execute OKD prep playbook
    * Execute OKD install playbook


|                            | AWS                       | Azure                             | GCP               |
| -------------------------- | ------------------------- | --------------------------------- | ----------------- |
| Region                     | us-west-2                 | westus2                           | us-west2 |
| Zone/AZ                    | us-west-2a                | westus2                           | us-west2-a | 
| Image id                   | ami-01ed306a12b7d1c96     | OpenLogic:CentOS:7.6:7.6.20190708 | projects/gce-uefi-images/global/images/centos-7-v20190619 |
| Bastion size               | t2.large (2 VCPUs/8GB)    | Standard_D2s_v3 (2 VCPU/8GB)      | n1-standard-2 (2 VCPU/7.5GB) |
| Master size                | t2.2xlarge (8 VCPUS/32GB) | Standard_E4s_v3 (4 VCPU/32GB)     | n1-highmem-4 (4 VCPU/26GB) |
| Install CLI (MacOS)        | pip3 install awscli --upgrade --user | brew update && brew install azure-cli | [Download CLI tarball](https://cloud.google.com/sdk/docs/quickstart-macos) |
| Config CLI                 | ~/.aws/credentials, ~/.aws/config | az login | gcloud init |
| Update CLI                 | pip3 install awscli --upgrade --user | brew update && brew upgrade azure-cli | gcloud components update |
| List zones/AZs             | aws ec2 describe-regions                                                                                                                          | az account list-locations                                                                                                                 | gcloud compute zones list | 
| Import SSH keypair         | aws ec2 import-key-pair --key-name "my-key" --public-key-material file://~/.ssh/my-key.pub                                                        | N/A                                                                                                                                       | gcloud compute config-ssh --ssh-key-file=~/.ssh/vm-ssh-key | 
| List VMs                   | aws ec2 describe-instances                                                                                                                        | az vm list                                                                                                                                | gcloud compute instances list |
| Show VM name and public IP | aws ec2 describe-instances --query "Reservations[].Instances[].{PublicIP:PublicIpAddress,Name:Tags[0].Value}" --output=table                      | az vm list-ip-addresses                                                                                                                   | gcloud compute instances list |
| Show VM details            | aws ec2 describe-instances --filters "Name=tag:Name,Values=okd-bastion"                                                                           | az vm show -n okd-bastion -g okd-test                                                                                                     | gcloud compute instances describe okd-bastion --zone="us-west2-a" | 
| Delete VM                  | aws ec2 terminate-instances --instance-ids i-0cf9dbe37e0e3d3fe                                                                                    | az vm delete -g centostest -n centostest                                                                                                  | gcloud compute instances delete okd-bastion --zone="us-west2-a" |  
| Deploy                     | aws cloudformation create-stack --stack-name okd-test --template-body file:///home/testuser/okd-test.json --parameters file://okd-test-param.json | az group deployment create --name okd-test --resource-group okd-test --template-file okd-test.json --parameters @okd-test.parameters.json | gcloud deployment-manager deployments create okd-test --template okd-test.jinja --properties zone:us-central1-a,machineType:n1-standard-1 | 
| Show status of deployment  | aws cloudformation describe-stacks --stack-name okd-test                                                                                          | az group deployment show --name okd-test --resource-group okd-test                                                                        | gcloud deployment-manager deployments describe okd-test |
| List deployments           | aws cloudformation describe-stacks                                                                                                                | az group deployment list --resource-group okd-test                                                                                        | gcloud deployment-manager deployments list |
| Delete deployment          | aws cloudformation delete-stack --stack-name okd-test                                                                                             | az group deployment delete --name okd-test --resource-group okd-test                                                                      | gcloud deployment-manager deployments delete okd-test |
