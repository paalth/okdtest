* Create infrastructure
  * Copy okdtest/azure/okd-test-parameters.json to azure/okd-test-parameters.json and update Azure params 
  * Create Azure resources including VMs - `okdtest/azure/createResource.sh`
  * Copy okdtest/azure/envVars.sh to azure/envVars, update params and source environment variables
* Config infrastructure
  * Copy files to bastion - `okdtest/azure/copyToBastion.sh`
  * SSH to bastion
  * Prepare bastion - run `prepare-bastion.sh`
  * cd to openshift-ansible directory
  * Prepare master - `ansible-playbook -i okd-inv-all-in-one -e @okd-test-vars.yaml prepare-master.yaml`
* Install OKD
  * Run OKD prep playbook - `ansible-playbook -i okd-inv-all-in-one -e @okd-test-vars.yaml playbooks/prerequisites.yml`
  * Run OKD install playbook - `ansible-playbook -i okd-inv-all-in-one -e @okd-test-vars.yaml playbooks/deploy_cluster.yml`
* Update Route53 wildcard record to point to master node public IP address
