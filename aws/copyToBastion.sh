cp okdtest/aws/okd-inv-all-in-one okd-inv-all-in-one.temp
cp okdtest/aws/okd-test-vars.yaml okd-test-vars.yaml.temp

AWS_OKD_BASTION_IP=`aws ec2 describe-instances --filters "Name=instance-state-name,Values=running" "Name=tag:Name,Values=okd-bastion" --query "Reservations[].Instances[].{PublicIP:PublicIpAddress}" --output=text`
AWS_OKD_MASTER_PUBLIC_NAME=`aws ec2 describe-instances --filters "Name=instance-state-name,Values=running" "Name=tag:Name,Values=okd-master" --query "Reservations[].Instances[].NetworkInterfaces[].Association.PublicDnsName" --output=text`
AWS_OKD_MASTER_PRIVATE_NAME=`aws ec2 describe-instances --filters "Name=instance-state-name,Values=running" "Name=tag:Name,Values=okd-master" --query "Reservations[].Instances[].NetworkInterfaces[].PrivateDnsName" --output=text`

echo "Bastion public IP   = $AWS_OKD_BASTION_IP"
echo "Master public name  = $AWS_OKD_MASTER_PUBLIC_NAME"
echo "Master private name = $AWS_OKD_MASTER_PRIVATE_NAME"

sed -i "s/AWS_OKD_MASTER_PRIVATE_NAME/$AWS_OKD_MASTER_PRIVATE_NAME/g" okd-inv-all-in-one.temp

sed -i "s/AWS_OKD_MASTER_PUBLIC_NAME/$AWS_OKD_MASTER_PUBLIC_NAME/g"   okd-test-vars.yaml.temp
sed -i "s/SSH_USER/$SSH_USER/g"                                       okd-test-vars.yaml.temp
sed -i "s/APPS_SUBDOMAIN/$APPS_SUBDOMAIN/g"                           okd-test-vars.yaml.temp
sed -i "s/ADMIN_USER/$ADMIN_USER/g"                                   okd-test-vars.yaml.temp
sed -i "s/ADMIN_PASSWORD/$ADMIN_PASSWORD/g"                           okd-test-vars.yaml.temp
sed -i "s/AWS_ACCESS_KEY/$AWS_ACCESS_KEY/g"                           okd-test-vars.yaml.temp
sed -i "s/AWS_SECRET_KEY/$AWS_SECRET_KEY/g"                           okd-test-vars.yaml.temp

scp ~/.ssh/id_rsa                      $SSH_USER@$AWS_OKD_BASTION_IP:.ssh
scp okd-inv-all-in-one.temp            $SSH_USER@$AWS_OKD_BASTION_IP:okd-inv-all-in-one
scp okd-test-vars.yaml.temp            $SSH_USER@$AWS_OKD_BASTION_IP:okd-test-vars.yaml
scp okdtest/common/prepare-bastion.sh  $SSH_USER@$AWS_OKD_BASTION_IP:prepare-bastion.sh
scp okdtest/common/prepare-master.yaml $SSH_USER@$AWS_OKD_BASTION_IP:prepare-master.yaml

rm -f okd-test-vars.yaml.temp
rm -f okd-inv-all-in-one.temp
