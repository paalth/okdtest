cp okdtest/azure/okd-inv-all-in-one okd-inv-all-in-one.temp
cp okdtest/azure/okd-test-vars.yaml okd-test-vars.yaml.temp

AZURE_OKD_BASTION_IP=`az vm list-ip-addresses -g okd-test -n okd-bastion --query [0].virtualMachine.network.publicIpAddresses[0].ipAddress -o tsv`
AZURE_OKD_MASTER_PUBLIC_IP=`az vm list-ip-addresses -g okd-test -n okd-master --query [0].virtualMachine.network.publicIpAddresses[0].ipAddress -o tsv`
AZURE_OKD_MASTER_PRIVATE_IP=`az vm list-ip-addresses -g okd-test -n okd-master --query [0].virtualMachine.network.privateIpAddresses[0] -o tsv`

AZURE_OKD_MASTER_PUBLIC_NAME=`echo $AZURE_OKD_MASTER_PUBLIC_IP | tr '.' '-'`
AZURE_OKD_MASTER_PRIVATE_NAME=`echo $AZURE_OKD_MASTER_PRIVATE_IP | tr '.' '-'`

AZURE_OKD_MASTER_PUBLIC_NAME=$AZURE_OKD_MASTER_PUBLIC_NAME'.nip.io'
AZURE_OKD_MASTER_PRIVATE_NAME=$AZURE_OKD_MASTER_PRIVATE_NAME'.nip.io'

echo "Bastion public IP   = $AZURE_OKD_BASTION_IP"
echo "Master public name  = $AZURE_OKD_MASTER_PUBLIC_NAME"
echo "Master private name = $AZURE_OKD_MASTER_PRIVATE_NAME"

sed -i "s/AZURE_OKD_MASTER_PRIVATE_NAME/$AZURE_OKD_MASTER_PRIVATE_NAME/g" okd-inv-all-in-one.temp

sed -i "s/AZURE_OKD_MASTER_PUBLIC_NAME/$AZURE_OKD_MASTER_PUBLIC_NAME/g"   okd-test-vars.yaml.temp
sed -i "s/SSH_USER/$SSH_USER/g"                                           okd-test-vars.yaml.temp
sed -i "s/APPS_SUBDOMAIN/$APPS_SUBDOMAIN/g"                               okd-test-vars.yaml.temp
sed -i "s/ADMIN_PASSWORD/$ADMIN_PASSWORD/g"                               okd-test-vars.yaml.temp

scp ~/.ssh/id_rsa                      $SSH_USER@$AZURE_OKD_BASTION_IP:.ssh
scp okd-inv-all-in-one.temp            $SSH_USER@$AZURE_OKD_BASTION_IP:okd-inv-all-in-one
scp okd-test-vars.yaml.temp            $SSH_USER@$AZURE_OKD_BASTION_IP:okd-test-vars.yaml
scp okdtest/common/prepare-bastion.sh  $SSH_USER@$AZURE_OKD_BASTION_IP:prepare-bastion.sh
scp okdtest/common/prepare-master.yaml $SSH_USER@$AZURE_OKD_BASTION_IP:prepare-master.yaml

rm -f okd-test-vars.yaml.temp
rm -f okd-inv-all-in-one.temp
