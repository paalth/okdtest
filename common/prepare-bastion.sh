chmod 400 ~/.ssh/id_rsa
sudo yum install -y git epel-release 
sudo yum install -y ansible
git clone https://github.com/openshift/openshift-ansible
cd openshift-ansible
git checkout release-3.11
mv ../okd-inv-all-in-one .
mv ../okd-test-vars.yaml .
mv ../prepare-master.yaml .
