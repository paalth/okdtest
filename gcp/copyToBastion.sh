cp okdtest/gcp/okd-inv-all-in-one okd-inv-all-in-one.temp
cp okdtest/gcp/okd-test-vars.yaml okd-test-vars.yaml.temp

GCP_OKD_BASTION_IP=`gcloud compute instances list --filter "Name:okd-bastion" --format="value(networkInterfaces[0].accessConfigs[0].natIP)"`
GCP_OKD_MASTER_PUBLIC_IP=`gcloud compute instances list --filter "Name:okd-master" --format="value(networkInterfaces[0].accessConfigs[0].natIP)"`
GCP_OKD_MASTER_PRIVATE_IP=`gcloud compute instances list --filter "Name:okd-master" --format="value(networkInterfaces[0].networkIP)"`

GCP_OKD_MASTER_PUBLIC_NAME=`echo $GCP_OKD_MASTER_PUBLIC_IP | tr '.' '-'`
GCP_OKD_MASTER_PRIVATE_NAME=`echo $GCP_OKD_MASTER_PRIVATE_IP | tr '.' '-'`

GCP_OKD_MASTER_PUBLIC_NAME=$GCP_OKD_MASTER_PUBLIC_NAME'.nip.io'
GCP_OKD_MASTER_PRIVATE_NAME=$GCP_OKD_MASTER_PRIVATE_NAME'.nip.io'

echo "Bastion public IP   = $GCP_OKD_BASTION_IP"
echo "Master public name  = $GCP_OKD_MASTER_PUBLIC_NAME"
echo "Master private name = $GCP_OKD_MASTER_PRIVATE_NAME"

sed -i "s/GCP_OKD_MASTER_PRIVATE_NAME/$GCP_OKD_MASTER_PRIVATE_NAME/g" okd-inv-all-in-one.temp

sed -i "s/GCP_OKD_MASTER_PUBLIC_NAME/$GCP_OKD_MASTER_PUBLIC_NAME/g"   okd-test-vars.yaml.temp
sed -i "s/SSH_USER/$SSH_USER/g"                                       okd-test-vars.yaml.temp
sed -i "s/APPS_SUBDOMAIN/$APPS_SUBDOMAIN/g"                           okd-test-vars.yaml.temp
sed -i "s/ADMIN_PASSWORD/$ADMIN_PASSWORD/g"                           okd-test-vars.yaml.temp
sed -i "s/GCP_PROJECT_ID/$GCP_PROJECT_ID/g"                           okd-test-vars.yaml.temp
sed -i "s/GCP_PREFIX/$GCP_PREFIX/g"                                   okd-test-vars.yaml.temp

scp -i ~/.ssh/google_compute_engine ~/.ssh/google_compute_engine       $SSH_USER@$GCP_OKD_BASTION_IP:.ssh/id_rsa
scp -i ~/.ssh/google_compute_engine okd-inv-all-in-one.temp            $SSH_USER@$GCP_OKD_BASTION_IP:okd-inv-all-in-one
scp -i ~/.ssh/google_compute_engine okd-test-vars.yaml.temp            $SSH_USER@$GCP_OKD_BASTION_IP:okd-test-vars.yaml
scp -i ~/.ssh/google_compute_engine okdtest/gcp/resizeDisk.sh          $SSH_USER@$GCP_OKD_BASTION_IP:resizeDisk.sh
scp -i ~/.ssh/google_compute_engine okdtest/common/prepare-bastion.sh  $SSH_USER@$GCP_OKD_BASTION_IP:prepare-bastion.sh
scp -i ~/.ssh/google_compute_engine okdtest/common/prepare-master.yaml $SSH_USER@$GCP_OKD_BASTION_IP:prepare-master.yaml

rm -f okd-test-vars.yaml.temp
rm -f okd-inv-all-in-one.temp
