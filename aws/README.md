* Create infrastructure
  * Copy okdtest/aws/okd-test-param.json to aws/okd-test-param.json and update AWS params 
  * Create AWS resources including VMs - `okdtest/aws/createResource.sh`
  * Copy okdtest/aws/envVars.sh to aws/envVars, update params and source environment variables
* Config infrastructure
  * Copy files to bastion - `okdtest/aws/copyToBastion.sh`
  * SSH to bastion
  * Prepare bastion - run `prepare-bastion.sh`
  * cd to openshift-ansible directory
  * Prepare master - `ansible-playbook -i okd-inv-all-in-one -e @okd-test-vars.yaml prepare-master.yaml`
* Install OKD
  * Run OKD prep playbook - `ansible-playbook -i okd-inv-all-in-one -e @okd-test-vars.yaml playbooks/prerequisites.yml`
  * Run OKD install playbook - `ansible-playbook -i okd-inv-all-in-one -e @okd-test-vars.yaml playbooks/deploy_cluster.yml`
* Update Route53 wildcard record to point to master node public IP address
