az group create --name okd-test --location "westus2"

az group deployment create \
  --name okd-test \
  --resource-group okd-test \
  --template-file okdtest/azure/okd-test-template.json \
  --parameters @azure/okd-test-parameters.json
