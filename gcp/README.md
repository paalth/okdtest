* Prerequisites
  * GCE project exists
  * SSH key added to project
  * VPC default network used, and firewall allows inbound traffic on ports 80,443,8443 to all instances
  * Default project service account granted access: gcloud projects add-iam-policy-binding <project id> --member serviceAccount:<project number>-compute@developer.gserviceaccount.com --role roles/compute.instanceAdmin
  * Default project service account granted access: gcloud projects add-iam-policy-binding <project id> --member serviceAccount:<project number>-compute@developer.gserviceaccount.com --role roles/iam.serviceAccountUser
* Create infrastructure
  * Copy okd-test-config.yaml to okd-test.yaml and update GCP params 
  * Create GCP resources - run `createResource.sh`
  * Copy okdtest/gcp/envVars.sh to gcp/envVars, update params and source environment variables
* Config infrastructure
  * Copy files to bastion - `okdtest/gcp/copyToBastion.sh`
  * SSH to bastion
  * Resize bastion disk - run `resizeDisk.sh`
  * Prepare bastion - run `prepare-bastion.sh`
  * SCP `resizeDisk.sh` to master
  * SSH to master
  * Resize master disk - run `resizeDisk.sh`
  * cd to openshift-ansible directory
  * Prepare master - `ansible-playbook -i okd-inv-all-in-one -e @okd-test-vars.yaml prepare-master.yaml`
* Install OKD
  * Run OKD prep playbook - `ansible-playbook -i okd-inv-all-in-one -e @okd-test-vars.yaml playbooks/prerequisites.yml`
  * Run OKD install playbook - `ansible-playbook -i okd-inv-all-in-one -e @okd-test-vars.yaml playbooks/deploy_cluster.yml`
* Update Route53 wildcard record to point to master node public IP address
